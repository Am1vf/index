lang = "en";

if (navigator.language == "es" || navigator.language == "es-ES") {
    lang = "es"
}

multiLangs = document.getElementsByClassName('multiLang');

that = this;

this.langChange();

function castellano() {
    lang = "es";
    this.langChange()
}

function english() {
    lang = "en";
    this.langChange()
}

function langChange() {
    for (let i = 0; i < multiLangs.length; i++) {
        if (multiLangs[i].lang !== lang) {
            multiLangs[i].style.display = "none"
        } else {
            multiLangs[i].style.display = "inherit"
        }
    }
}

// spinner
document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
        document.getElementById('load').style.visibility = "visible";
    }
    if (state == 'complete') {
        document.getElementById('load').style.opacity = 0;
        document.getElementById('load').style.visibility = "hidden";
    }
}